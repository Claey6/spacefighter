
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	// checks the delaey time between the last enemy spawn
	if (m_delaySeconds > 0)
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		// spawns another enemy if the delay time has passed
		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	// checks if the ship is active
	if (IsActive())
	{
		// if the enemy ship is still on the screen (hasn't been destroyed via collision), checks total time that the ship has been on screen
		m_activationSeconds += pGameTime->GetTimeElapsed();
		// deactivates the ship after a set time or if has moved off the screen
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	// calls function run again, updating the ship location on screen
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}